package com.tencent.wxcloudrun.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class BandInfo implements Serializable {

  private int id;
  private String name;
  private String url;
  private String avatar;
  private int vote;
}
