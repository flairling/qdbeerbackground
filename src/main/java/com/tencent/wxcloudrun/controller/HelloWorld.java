package com.tencent.wxcloudrun.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tencent.wxcloudrun.config.ApiResponse;

/**
 * HelloWorld
 */
@RestController
public class HelloWorld {

  @RequestMapping("/helloworld")
  @ResponseBody
  ApiResponse helloWorld(@RequestBody String str) {
    return ApiResponse.ok("notok");
  }

}